#
# Copyright (C) 2018-2019 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#
$(call inherit-product, $(SRC_TARGET_DIR)/product/product_launched_with_p.mk)
$(call inherit-product, device/oneplus/sm8150-common/common.mk)

# Get non-open-source specific aspects
$(call inherit-product, vendor/oneplus/guacamole/guacamole-vendor.mk)

# Overlays
DEVICE_PACKAGE_OVERLAYS += \
    $(LOCAL_PATH)/overlay \
    $(LOCAL_PATH)/overlay-lineage

# A/B firmware
AB_OTA_PARTITIONS += \
    abl \
    aop \
    bluetooth \
    cmnlib64 \
    cmnlib \
    devcfg \
    dsp \
    hyp \
    keymaster \
    LOGO \
    modem \
    qupfw \
    storsec \
    tz \
    xbl_config \
    xbl

# Below is a pseudo A/B partition
AB_OTA_PARTITIONS += \
    oem_stanvbk

# Audio
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/audio/mixer_paths_pahu.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths_pahu.xml \
    $(LOCAL_PATH)/audio/mixer_paths_tavil.xml:$(TARGET_COPY_OUT_VENDOR)/etc/mixer_paths_tavil.xml

# Camera
PRODUCT_PACKAGES += \
    OnePlusCameraHelper

# Device init scripts
PRODUCT_PACKAGES += \
    fstab.qcom \
    init.display.guacamole.rc

# OPFeature
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/configs/odm_feature_list:$(TARGET_COPY_OUT_ODM)/etc/odm_feature_list

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

PRODUCT_PROPERTY_OVERRIDES += ro.apex.updatable=false

# Opengapps
GAPPS_VARIANT := nano
$(call inherit-product-if-exists, vendor/opengapps/build/opengapps-packages.mk)

define target-radio-files $(notdir \
	$(wildcard $(LOCAL_PATH)/firmware/filesmap) \
	$(wildcard $(LOCAL_PATH)/firmware/*.bin) \
	$(wildcard $(LOCAL_PATH)/firmware/*.elf) \
	$(wildcard $(LOCAL_PATH)/firmware/*.img) \
	$(wildcard $(LOCAL_PATH)/firmware/*.mbn) \
	)
endef
